import time

import numpy as np
import tensorflow as tf
from data_processing import sortCleanQuestions, answerWordToInt, questionWordToInt, sortCleanAnswers, \
    answerWordToIntToWord, cleanText


# ---------- PART 2 - BUILDING THE SEQ2SEQ MODEL ----------
# NTT TODO Tensorflow document
# http://vietonrails.com/ai/2016/05/13/co-ban-ve-tensorflow
# https://kipalog.com/posts/Bat-dau-voi-Machine-Learning-thong-qua-Tensorflow--Phan-I-2/
# Creating placeholders for the inputs and the targets


def modelInputs():  # NTT TODO chưa hiểu lắm cần xem lại
    inputs = tf.placeholder(tf.int32, [None, None], name="input")
    targets = tf.placeholder(tf.int32, [None, None], name="target")
    lr = tf.placeholder(tf.float32,
                        name="learning_rate")  # NTT TODO liên quan tf.nn.bidirectional_dynamic_rnn(... error ValueError: Tensor conversion requested dtype float32 for Tensor with dtype int32: 'Tensor("keep_prob:0", dtype=int32)'
    keepProb = tf.placeholder(tf.float32,
                              name="keep_prob")  # NTT TODO liên quan tf.nn.bidirectional_dynamic_rnn(... error ValueError: Tensor conversion requested dtype float32 for Tensor with dtype int32: 'Tensor("keep_prob:0", dtype=int32)'
    return inputs, targets, lr, keepProb


# Processing the target
def preprocessTargets(targets, wordToInt, batchSize):
    leftSize = tf.fill([batchSize, 1], wordToInt['<SOS>'])
    rightSize = tf.strided_slice(targets, [0, 0], [batchSize, -1], [1, 1])
    preprocessedTargets = tf.concat([leftSize, rightSize], 1)
    return preprocessedTargets


# Creating the Encoder RNN
def encoderRnn(rnnInputs, rnnSize, numberLayers, keepProb, sequenceLength):
    lstm = tf.contrib.rnn.BasicLSTMCell(rnnSize)
    lstmDropout = tf.contrib.rnn.DropoutWrapper(lstm, input_keep_prob=keepProb)
    encoderCell = tf.contrib.rnn.MultiRNNCell([lstmDropout] * numberLayers)
    encoderOuput, encoderState = tf.nn.bidirectional_dynamic_rnn(cell_fw=encoderCell,
                                                                 cell_bw=encoderCell,
                                                                 sequence_length=sequenceLength,
                                                                 inputs=rnnInputs,
                                                                 dtype=tf.float32)
    return encoderState


# Decoding the training set
def decodeTrainingSet(encoderState, decoderCell, decoderEmbeddedInput, sequenceLength, decodingScope, outputFunction,
                      keepProb, batchSize):
    attentionStates = tf.zeros([batchSize, 1, decoderCell.output_size])
    attentionKeys, attentionValue, attentionScoreFunction, attentionContructFunction = tf.contrib.seq2seq.prepare_attention(
        attentionStates, attention_option="bahdanau", num_units=decoderCell.output_size)
    trainningDecoderFunction = tf.contrib.seq2seq.attention_decoder_fn_train(encoderState[0],
                                                                             attentionKeys,
                                                                             attentionValue,
                                                                             attentionScoreFunction,
                                                                             attentionContructFunction,
                                                                             name="attn_dec_train")
    decoderOuput, decoderFinalState, decoderFinalContextState = tf.contrib.seq2seq.dynamic_rnn_decoder(decoderCell,
                                                                                                       trainningDecoderFunction,
                                                                                                       decoderEmbeddedInput,
                                                                                                       sequenceLength,
                                                                                                       scope=decodingScope)
    decoderOutputDropout = tf.nn.dropout(decoderOuput, keepProb)
    return outputFunction(decoderOutputDropout)


# Decoding the test/validation set
def decodeTestSet(encoderState, decoderCell, decoderEmbeddedMatrix, sosId, eosId, maximumLength, numberWord,
                  decodingScope, outputFunction, keepProb, batchSize):
    attentionStates = tf.zeros([batchSize, 1, decoderCell.output_size])
    attentionKeys, attentionValue, attentionScoreFunction, attentionContructFunction = tf.contrib.seq2seq.prepare_attention(
        attentionStates, attention_option="bahdanau",
        num_units=decoderCell.output_size)  # https://www.tensorflow.org/versions/r1.0/api_docs/python/tf/contrib/seq2seq/prepare_attention
    testDecoderFunction = tf.contrib.seq2seq.attention_decoder_fn_inference(outputFunction,
                                                                            encoderState[0],
                                                                            attentionKeys,
                                                                            attentionValue,
                                                                            attentionScoreFunction,
                                                                            attentionContructFunction,
                                                                            decoderEmbeddedMatrix,
                                                                            sosId,
                                                                            eosId,
                                                                            maximumLength,
                                                                            numberWord,
                                                                            name="attn_dec_inf")
    testPredictions, decoderFinalState, decoderFinalContextState = tf.contrib.seq2seq.dynamic_rnn_decoder(decoderCell,
                                                                                                          testDecoderFunction,
                                                                                                          scope=decodingScope)
    return testPredictions


# Creating the decoder RNN
def decoderRnn(decoderEmbeddedInput, decoderEmbeddedMatrix, encoderState, numberWord, sequenceLength, rnnSize,
               numberLayer, wordToInt, keepProb, batchSize):
    with tf.variable_scope("decoding") as decodingScope:
        lsmt = tf.contrib.rnn.BasicLSTMCell(rnnSize)
        lsmtDropout = tf.contrib.rnn.DropoutWrapper(lsmt, input_keep_prob=keepProb)
        decoderCell = tf.contrib.rnn.MultiRNNCell([lsmtDropout] * numberLayer)
        weights = tf.truncated_normal_initializer(stddev=0.1)
        biases = tf.zeros_initializer()
        outputFunction = lambda x: tf.contrib.layers.fully_connected(x,
                                                                     numberWord,
                                                                     None,
                                                                     scope=decodingScope,
                                                                     weights_initializer=weights,
                                                                     biases_initializer=biases)
        trainningPredictions = decodeTrainingSet(encoderState,
                                                 decoderCell,
                                                 decoderEmbeddedInput,
                                                 sequenceLength,
                                                 decodingScope,
                                                 outputFunction,
                                                 keepProb,
                                                 batchSize)
        decodingScope.reuse_variables()
        testPredictions = decodeTestSet(encoderState,
                                        decoderCell,
                                        decoderEmbeddedMatrix,
                                        wordToInt['<SOS>'],
                                        wordToInt['<EOS>'],
                                        sequenceLength - 1,
                                        numberWord,
                                        decodingScope,
                                        outputFunction,
                                        keepProb,
                                        batchSize)
    return trainningPredictions, testPredictions


# Building the seq2seq model
def sequenceToSequenceModel(inputs, targets, keepProb, batchsize, seqenceLength, answersNumWord, questionNumWords,
                            encoderEmbeddedSize, decoderEmbeddedSize, rnnSize, numLayers, questionWordToInt):
    encoderEmbeddedInputs = tf.contrib.layers.embed_sequence(inputs,
                                                             answersNumWord + 1,
                                                             encoderEmbeddedSize,
                                                             initializer=tf.random_uniform_initializer(0, 1))
    encoderState = encoderRnn(encoderEmbeddedInputs, rnnSize, numLayers, keepProb, sequenceLength)
    preprocessedTargets = preprocessTargets(targets, questionWordToInt, batchsize)
    decoderEmbeddedMatrix = tf.Variable(tf.random_uniform([questionNumWords + 1, decoderEmbeddedSize], 0, 1))
    decoderEmbeddedInputs = tf.nn.embedding_lookup(decoderEmbeddedMatrix, preprocessedTargets)
    trainingPredictions, testPredictions = decoderRnn(decoderEmbeddedInputs,
                                                      decoderEmbeddedMatrix,
                                                      encoderState,
                                                      questionNumWords,
                                                      seqenceLength,
                                                      rnnSize,
                                                      numLayers,
                                                      questionWordToInt,
                                                      keepProb,
                                                      batchsize)
    return trainingPredictions, testPredictions


#	---------- PART 3 - TRAINING THE SEQ2SEQ MODEL ----------

# Setting the Hyperparameters
epochs = 2  # 100
# NTT TODO batchSize must be less count size data train input
batchSize = 2  # 32   2train for 10
rnnSize = 1024
numLayer = 3
encodingEmbeddingSize = 1024
decodingEmbeddingSize = 1024
learningRate = 0.01
learningRateDecay = 0.9
minLearningRate = 0.0001
keepProbability = 0.5

# Defining a session
tf.reset_default_graph()  # NTT Adding fix
session = tf.InteractiveSession()

# Loading the model inputs
inputs, targets, lr, keepProb = modelInputs()

# Setting the sequence length
sequenceLength = tf.placeholder_with_default(25, None, name="sequence_length")  # 25 in  for length in range(1,25+1):

# Getting the shape of the inputs tensor
inputShape = tf.shape(inputs)

# Getting the training and test predictions
trainingPredictions, testPredictions = sequenceToSequenceModel(tf.reverse(inputs, [-1]),
                                                               targets,
                                                               keepProb,
                                                               batchSize,
                                                               sequenceLength,
                                                               len(answerWordToInt),
                                                               len(questionWordToInt),
                                                               encodingEmbeddingSize,
                                                               decodingEmbeddingSize,
                                                               rnnSize,
                                                               numLayer,
                                                               questionWordToInt)

# Setting up the Loss Error, the Optimizer and Gradient Clipping
with tf.name_scope("optimization"):
    lostError = tf.contrib.seq2seq.sequence_loss(trainingPredictions, targets, tf.ones([inputShape[0], sequenceLength]))
    optimizer = tf.train.AdamOptimizer(learningRate)
    gradients = optimizer.compute_gradients(lostError)
    clippedGradients = [(tf.clip_by_value(gradTensor, -5., 5.), gradVari) for gradTensor, gradVari in gradients if
                        gradTensor is not None]
    optimizerGradientClipping = optimizer.apply_gradients(clippedGradients)


# Padding sequence with the <PAD> token
# Question [who, are, you]
# Answer [<SOS>, I, am, a, bot, ., <EOS>]
def applyPadding(batchOfSequences, wordToInt):
    maxSequeceLength = max([len(sequence) for sequence in batchOfSequences])
    return [sequence + [wordToInt["<PAD>"]] * (maxSequeceLength - len(sequence)) for sequence in batchOfSequences]


# Splitting the data into batches of questions and answers
def splitIntoBatches(questions, answers, batchSize):
    for batchIndex in range(0, len(questions) // batchSize):
        startIndex = batchIndex * batchSize
        questionInBatch = questions[startIndex:startIndex + batchSize]
        answerInBatch = answers[startIndex:startIndex + batchSize]
        paddedQuestionsInBatch = np.array(applyPadding(questionInBatch, questionWordToInt))
        paddedAnswersInBatch = np.array(applyPadding(answerInBatch, answerWordToInt))
        yield paddedQuestionsInBatch, paddedAnswersInBatch


# Splitting the questions and answers into training and validation sets
trainingValidationSplit = int(len(sortCleanQuestions) * 0.15)
trainingQuestion = sortCleanQuestions[trainingValidationSplit:]
trainingAnswer = sortCleanAnswers[trainingValidationSplit:]
validationQuestion = sortCleanQuestions[:trainingValidationSplit]
validationAnswer = sortCleanAnswers[:trainingValidationSplit]

# Training
batchIndexCheckTrainingLoss = 100
batchIndexCheckValidationLoss = ((len(trainingQuestion)) // batchSize // 2) - 1
totalTrainingLostError = 0
listValidationLostError = []
earlyStoppingCheck = 0
earlyStoppingStop = 1000
checkPoint = "./trains/chatbot_weights.ckpt"
session.run(tf.global_variables_initializer())
for epoch in range(1, epochs + 1):  # run 1 to epochs=100 Epoch
    for batchIndex, (paddedQuestionInBatch, paddedAnswerInBatch) in enumerate(
            splitIntoBatches(trainingQuestion, trainingAnswer, batchSize)):
        startingTime = time.time()
        _, batchTrainingLossError = session.run([optimizerGradientClipping, lostError],
                                                {inputs: paddedQuestionInBatch,
                                                 targets: paddedAnswerInBatch,
                                                 lr: learningRate,
                                                 sequenceLength: paddedAnswerInBatch.shape[1],
                                                 keepProb: keepProbability})
        totalTrainingLostError += batchTrainingLossError
        endingTime = time.time()
        batchTime = endingTime - startingTime
        if batchIndex % batchIndexCheckTrainingLoss == 0:
            print(
                "Epoch: {:>3}/{}, Batch: {:>4}/{}, Training Loss Error: {:>6.3f}, Training Time on 100 Batches: {:d} seconds".format(
                    epoch,
                    epochs,
                    batchIndex,
                    len(trainingQuestion) // batchSize,
                    totalTrainingLostError / batchIndexCheckTrainingLoss,
                    int(batchTime * batchIndexCheckTrainingLoss))
            )
            totalTrainingLostError = 0
            print("NTT continute validate errror :: batchIndex {:>6.3f}  batchIndexCheckValidationLoss {:d} ".format(
                batchIndex, batchIndexCheckValidationLoss))
        if batchIndex % batchIndexCheckValidationLoss == 0 and batchIndex > 0:
            print("NTT continute validate :::")
            totalValidationLossError = 0
            startingTime = time.time()
            for batchIndexValidation, (paddedQuestionInBatch, paddedAnswerInBatch) in enumerate(
                    splitIntoBatches(validationQuestion, validationAnswer, batchSize)):
                batchValidationLossError = session.run(lostError, {inputs: paddedQuestionInBatch,
                                                                   targets: paddedAnswerInBatch,
                                                                   lr: learningRate,
                                                                   sequenceLength: paddedAnswerInBatch.shape[1],
                                                                   keepProb: 1})
                totalValidationLossError += batchTrainingLossError
            endingTime = time.time()
            batchTime = endingTime - startingTime
            averageValidationLossError = totalValidationLossError / (len(validationQuestion) / batchSize)
            print("Validation Loss Error: {:>6.3f}, Batch Validation Time: {:d} seconds".format(
                averageValidationLossError, int(batchTime)))  # https://pyformat.info
            learningRate *= learningRateDecay
            if learningRate < minLearningRate:
                learningRate = minLearningRate
            listValidationLostError.append(averageValidationLossError)
            if averageValidationLossError <= min(listValidationLostError):
                print("I speak better now!!")
                earlyStoppingCheck = 0
                saver = tf.train.Saver()
                saver.save(session, checkPoint)
            else:
                print("Sorry I do not speak better, I need to practice more.")
                earlyStoppingCheck += 1
                if earlyStoppingCheck == earlyStoppingStop:
                    break
    if earlyStoppingCheck == earlyStoppingStop:
        print("My apologies, I cannot speak better anymore. This is a best I can do.")
        break
print("Game Over")

#	---------- PART 4 - TESTING THE SEQ2SEQ MODEL ----------

# Loading the wights and Running the session
checkPoints = "./trains/chatbot_weights.ckpt"
session = tf.InteractiveSession()
session.run(tf.global_variables_initializer())
saver = tf.train.Saver()
saver.restore(session, checkPoints)


# Converting the questions from strings to lists of encoding integers
def convertStringToInt(question, wordToInt):
    question = cleanText(question)
    return [wordToInt.get(word, wordToInt['<OUT>']) for word in question.split()]


# Setting up the chat
while (True):
    question = input("You: ")
    if question == "GoodBye":
        break
    question = convertStringToInt(question, questionWordToInt)
    print("     NTT Chatting vector ::: ", question)
    question += [questionWordToInt['<PAD>']] * (25 - len(question))
    fakeBatch = np.zeros((batchSize, 25))
    fakeBatch[0] = question
    predictionAnswers = session.run(testPredictions, {inputs: fakeBatch, keepProb: 0.5})[0]
    answer = ''
    for i in np.argmax(predictionAnswers, 1):
        if answerWordToIntToWord[i] == 'i':
            token = "I"
        elif answerWordToIntToWord[i] == '<EOS>':
            token = "."
        elif answerWordToIntToWord[i] == '<OUT>':
            token = "out"
        else:
            token = " " + answerWordToIntToWord[i]
        answer += token
        if token == ".":
            break
    print("ChatBots: " + answer)
