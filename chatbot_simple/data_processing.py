import re

#  ---------- PART 1 - DATA PREPROCESSING ----------
# Import the dataset
lines = open("movie_lines.txt", encoding="utf-8", errors="ignore").read().split('\n')
conversations = open("movie_conversations.txt", encoding="utf-8", errors="ignore").read().split('\n')
# NTT print("NTT test input print: ", lines)
# Creating a dictionary that maps each line and its id
idToLine = {}
for line in lines:
    _lines = line.split(" +++$+++ ")
    if len(_lines) == 5:
        idToLine[_lines[0]] = _lines[4]

# Creating a list of all of conversations
conversationsIds = []
for conversation in conversations[:-1]:  # [:-1] as [0:-1],  [:-1]  except last line,,
    _conversation = conversation.split(" +++$+++ ")[-1][1:-1].replace("'", "").replace(" ", "")
    conversationsIds.append(_conversation.split(','))

# Getting separately the questions and the answers
questions = []
answers = []
for conversation in conversationsIds:
    for i in range(len(conversation) - 1):
        questions.append(idToLine[conversation[i]])
        answers.append(idToLine[conversation[i + 1]])


# Doing a first cleaning of the text
def cleanText(text):
    text = text.lower()
    text = re.sub(r"i'm", "i am", text)
    text = re.sub(r"he's", "he is", text)
    text = re.sub(r"she's", "she is", text)
    text = re.sub(r"that's", "that is", text)
    text = re.sub(r"what's", "what is", text)
    text = re.sub(r"where's", "where is", text)
    text = re.sub(r"how's", "how is", text)
    text = re.sub(r"\'ll", " will", text)
    text = re.sub(r"\'ve", " have", text)
    text = re.sub(r"\'re", " are", text)
    text = re.sub(r"\'d", " would", text)
    text = re.sub(r"n't", " not", text)
    text = re.sub(r"won't", "will not", text)
    text = re.sub(r"can't", "cannot", text)
    text = re.sub(r"[-()\"#/@;:<>{}`+=~|.!?,]", "", text)
    return text


# Cleaning the questions
cleanQuestions = []
for question in questions:
    cleanQuestions.append(cleanText(question))
# Cleaning the answers
cleanAnswers = []
for answer in answers:
    cleanAnswers.append(cleanText(answer))

# Filtering out the questions and answers that are too short or too long  # NTT Adding fix error
short_questions = []
short_answers = []
i = 0
for question in cleanQuestions:
    if 2 <= len(question.split()) <= 25:
        short_questions.append(question)
        short_answers.append(cleanAnswers[i])
    i += 1
cleanQuestions = []
cleanAnswers = []
i = 0
for answer in short_answers:
    if 2 <= len(answer.split()) <= 25:
        cleanAnswers.append(answer)
        cleanQuestions.append(short_questions[i])
    i += 1

# Creating dictionary that maps each word to its number of occurrences    NTT count word IMPORT  cleanQuestions   cleanAnswers
wordToCount = {}
for question in cleanQuestions:
    for word in question.split(" "):
        if (word not in wordToCount):
            wordToCount[word] = 1
        else:
            wordToCount[word] += 1
# NTT co gi do sai sai, tai sao lai lap lai, theo xac suat thong ke cung ok
for answer in cleanAnswers:
    for word in answer.split(" "):
        if (word not in wordToCount):
            wordToCount[word] = 1
        else:
            wordToCount[word] += 1

# Creating two dictionaries that map the questions words and the answers words to a unique integer
threshold = 15
questionWordToInt = {}
wordNumber = 0
for word, count in wordToCount.items():
    if (count >= threshold):
        questionWordToInt[word] = wordNumber
        wordNumber += 1

answerWordToInt = {}
wordNumber = 0
for word, count in wordToCount.items():
    if (count >= threshold):
        answerWordToInt[word] = wordNumber
        wordNumber += 1

# Adding the last tokens to these two dictionaries
tokens = ['<PAD>', '<EOS>', '<OUT>', '<SOS>']  # EOS == .
for token in tokens:
    questionWordToInt[token] = len(questionWordToInt) + 1
for token in tokens:
    answerWordToInt[token] = len(answerWordToInt) + 1

# Creating the inverse dictionary of the answerWordToInt dictionary
answerWordToIntToWord = {w_i: w for w, w_i in answerWordToInt.items()}  # NTT TODO cach viet nay la the nao ta ta

# Adding End Of String token to end every answer
for i in range(len(cleanAnswers)):
    cleanAnswers[i] += ' <EOS>'  # NTT tai sao space

# Translating all the questions and answers into integer
# and Replacing all the words that were filtered out by <OUT>
questionIntoInt = []
for question in cleanQuestions:
    ints = []
    for word in question.split():
        if word not in questionWordToInt:
            ints.append(questionWordToInt['<OUT>'])
        else:
            ints.append(questionWordToInt[word])
    questionIntoInt.append(ints)

answerIntoInt = []
for answer in cleanAnswers:
    ints = []
    for word in answer.split():
        if word not in answerWordToInt:
            ints.append(answerWordToInt['<OUT>'])
        else:
            ints.append(answerWordToInt[word])
    answerIntoInt.append(ints)

# NTT IMPORTANT ==>  OUT vector word


# Sorting questions and answers by the length of questions
sortCleanQuestions = []  # NTT vector question
sortCleanAnswers = []
for length in range(1, 25 + 1):  # NTT TODO why biết được độ dài mã và tại sao ghi 25+1  ChatBot - Step 17
    for i in enumerate(questionIntoInt):
        if len(i[1]) == length:
            sortCleanQuestions.append(questionIntoInt[i[0]])
            sortCleanAnswers.append(answerIntoInt[i[0]])

print("Finsh PART 1 data question size ::", len(sortCleanQuestions), "  answer size ::", len(sortCleanAnswers))

# NTT TODO export sortCleanQuestions, answerWordToInt, questionWordToInt, sortCleanAnswers, answerWordToIntToWord
